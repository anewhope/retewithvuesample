import { minifyHtml, injectHtml } from 'vite-plugin-html'
import legacy from '@vitejs/plugin-legacy'
import path from 'path'
import { createVuePlugin } from 'vite-plugin-vue2'

// https://vitejs.dev/config/
export default {
  plugins: [
    createVuePlugin(),
    minifyHtml(),
    injectHtml({
      data: {
        title: 'ReteNodeVisualization',
        description: 'An sample rete project using VueJS'
      }
    }),
    legacy({
      targets: ['ie >= 11'],
      additionalLegacyPolyfills: ['regenerator-runtime/runtime']
    })
  ],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, '/src'),
      '~bootstrap': 'bootstrap'
    }
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `@import "./src/scss/variables";`
      }
    }
  }
}
