import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: <div></div>
  }
]

const router = new VueRouter({
  linkActiveClass: 'active',
  routes
})

export default router