import { BehaviorSubject, Subject } from "rxjs";

export default class {
  constructor() {
    this.configurator = new BehaviorSubject(null);
    this.dialogState = new Subject();
  }
}
