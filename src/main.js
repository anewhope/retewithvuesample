import Vue from "vue"
import "regenerator-runtime/runtime.js"
import Provider from "./Provider.vue"
import VueRx from "vue-rx"
import VTooltip from "v-tooltip"

Vue.config.productionTip = false

Vue.use(VueRx)
Vue.use(VTooltip)

new Vue({
  render: (h) => h(Provider)
}).$mount('#app')
